module libload;

import bindbc.raylib : RaylibSupport, raylibSupport, loadRaylib;
import std.stdio : writeln;

/** 
 * Load Raylib library
 * Returns:
 *  true if Raylib loaded correctly
 */
bool loadedRaylib() {
    // Attempt to load the Raylib library
    RaylibSupport retVal = loadRaylib();

    // Check if the library loaded successfully
    if (retVal != raylibSupport) {
        // Print an error message if loading failed
        writeln("*ERROR* ", retVal);
        return false;
    }
    
    // Raylib loaded successfully
    return true;
}