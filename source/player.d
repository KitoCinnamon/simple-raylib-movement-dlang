module player;
import bindbc.raylib : Vector2, Texture, DrawTextureEx, UnloadTexture, RAYWHITE, IsKeyDown, KEY_A, KEY_D, KEY_W, KEY_S;
/** 
 * Player class
 */
class Player {
    public:
        this(Vector2 position, Texture sprite, float speed) {
            // Initialize player properties
            this.position = position;
            this.sprite = sprite;
            this.speed = speed;
        }

        /** 
         * Update the player instance
         * Params:
         *   deltaTime = time since last update
         */
        void update(float deltaTime) {
            // Update player position based on input and deltaTime
            if (IsKeyDown(KEY_A))
                position.x -= this.speed * deltaTime;
            if (IsKeyDown(KEY_D))
                position.x += this.speed * deltaTime;
            if (IsKeyDown(KEY_W))
                position.y -= this.speed * deltaTime;
            if (IsKeyDown(KEY_S))
                position.y += this.speed * deltaTime;
        }

        /** 
         * Draw the player instance
         */
        void draw() {
            // Draw the player sprite at its position
            DrawTextureEx(this.sprite, this.position, 0, 0.2f, RAYWHITE);
        }

    private:
        Vector2 position; // Player position
        Texture sprite;   // Player sprite
        float speed;      // Player movement speed
}