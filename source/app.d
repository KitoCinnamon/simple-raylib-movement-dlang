import libload : loadedRaylib;
import std.stdio : writeln;
import bindbc.raylib : InitWindow, SetTargetFPS, WindowShouldClose, RAYWHITE, CloseWindow, LoadTexture, UnloadTexture,
GetFrameTime, Vector2, Texture, ClearBackground, BeginDrawing, EndDrawing, DrawText, BLACK;
import player : Player;

void main() {
    // Check if Raylib library loaded successfully
    if (!loadedRaylib()) {
        return;
    }

    const int screenWidth = 800;
    const int screenHeight = 600;

    // Initialize window and set target FPS
    InitWindow(screenWidth, screenHeight, "Raylib Movement - D");
    SetTargetFPS(60);
    
    float deltaTime;
    Vector2 position = {0, 0};
    // Load player sprite
    Texture sprite = LoadTexture("resources/mackenzie_plush.png");
    const float speed = 400.3f;
    // Create player instance
    Player player = new Player(position, sprite, speed);

    // Main game loop
    while (!WindowShouldClose()) {
        // Calculate deltaTime for frame
        deltaTime = GetFrameTime();
        // Update player
        player.update(deltaTime);

        // Begin drawing
        BeginDrawing();
        // Clear background
        ClearBackground(RAYWHITE);
        // Draw text
        DrawText("Mackenzie Movement", 250, screenHeight / 2, 50, BLACK);
        // Draw player
        player.draw();

        // End drawing
        EndDrawing();
    }

    // Unload player sprite
    UnloadTexture(sprite);
    // Close window
    CloseWindow();
}