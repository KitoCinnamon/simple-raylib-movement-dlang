# Simple Raylib Movement (Dlang)

This repository contains a simple example of player movement using the Raylib library in D.

## Usage

1. Ensure you have DUB installed. If not, you can install it following the instructions [here](https://code.dlang.org/download).

2. Clone this repository to your local machine:
    ```
    git clone https://codeberg.org/Cyrodwd/simple-raylib-movement-dlang.git
    ```

3. Navigate to the cloned repository:
    ```
    cd simple-raylib-movement-dlang
    ```

4. Build and run the example using DUB:
    ```
    dub run
    ```

5. You should see a window displaying with a sprite. You can control the movement of the sprite using WASD keys.

6. Press `Esc` or close the window to exit the program.

## Dependencies

This project depends on the `bindbc-raylib3` package to interface with the Raylib library. DUB will automatically handle downloading and managing this dependency for you.

## Contributing

It's very simple, but if you want to contribute, it's fine.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.
